package org.bb.crud.api;

import java.util.List;

import org.bb.crud.model.mysql.Station;
import org.bb.crud.service.MasterDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/master")
public class StationMasterRestController {

	Logger log = LoggerFactory.getLogger(StationMasterRestController.class);

	@Autowired
	private MasterDataService masterDataService;

	@GetMapping(value = "/station")
	public List<Station> getAllStation() {
		return masterDataService.getAllStation();
	}

	@GetMapping(value = "/test")
	public String testing() {
		return "tested";
	}
}

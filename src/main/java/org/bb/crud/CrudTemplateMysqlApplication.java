package org.bb.crud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@EnableEurekaClient
@PropertySource(value = { "classpath:${spring.profiles.active}/application.properties", "classpath:${spring.profiles.active}/db.properties" })
public class CrudTemplateMysqlApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrudTemplateMysqlApplication.class, args);
	}
}

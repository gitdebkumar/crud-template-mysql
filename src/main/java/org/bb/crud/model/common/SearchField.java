package org.bb.crud.model.common;

public class SearchField {

	private String fieldName;
	private String fieldValue;
	private MatchType matchType;

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getFieldValue() {
		return fieldValue;
	}

	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}

	public MatchType getMatchType() {
		return matchType;
	}

	public void setMatchType(MatchType matchType) {
		this.matchType = matchType;
	}

	public enum MatchType {
		EQUAL, NOTEQUAL, LIKE, NOTLIKE, GT, GTE, LT, LTE
	}
}

package org.bb.crud.model.common;

import java.util.Collection;

import org.springframework.data.domain.Page;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ApiCollectionDto<T> extends ResponseDto {

	private int count = 0;
	private long totalCount = 0;
	private int pageSize;
	private int pageNo;
	private Collection<T> data;

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public Collection<T> getData() {
		return data;
	}

	public void setData(Collection<T> data) {
		this.data = data;
	}

	public long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(long totalCount) {
		this.totalCount = totalCount;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public ApiCollectionDto() {
		super();
	}

	public ApiCollectionDto(long totalCount, Collection<T> data) {
		super();
		this.totalCount = totalCount;
		this.data = data;
		
		this.count = data.size();
	}

	public ApiCollectionDto(Page<T> data) {
		super();
		this.count = data.getNumberOfElements();
		this.totalCount = data.getTotalElements();
		this.pageSize = data.getSize();
		this.pageNo = data.getNumber();
		this.data = data.getContent();		
	}

	public ApiCollectionDto(int count, long totalCount, int pageSize, int pageNo, Collection<T> data) {
		super();
		this.totalCount = totalCount;
		this.pageSize = pageSize;
		this.pageNo = pageNo;
		this.data = data;
		
		this.count = data.size();
	}
}

package org.bb.crud.model.mysql;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "station_master")
public class Station implements Serializable {

	private static final long serialVersionUID = 7686437589318756552L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "order_id")
    private Integer id;

    @Column(name = "station_code", nullable = false)
    private String stationCode;
    
    @Column(name = "station_name_english", nullable = false)
    private String stationName;
}

package org.bb.crud.config;

import org.bb.crud.model.common.ErrorResponseDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

	Logger log = LoggerFactory.getLogger(GlobalExceptionHandler.class);

//	@ExceptionHandler(AuthenticationException.class)
//	protected ResponseEntity<ErrorResponseDto> handleAuthenticationException(Exception ex, WebRequest request) {
//		ErrorResponseDto err = new ErrorResponseDto();
//		err.setMessage(ex.getMessage());
//		err.setStatus("FAILED");
//		log.error("Authentication error occured", ex);
//		return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(err);
//	}
//
//	@ExceptionHandler(AccessDeniedException.class)
//	protected ResponseEntity<ErrorResponseDto> handleAccessDeniedException(Exception ex, WebRequest request) {
//		ErrorResponseDto err = new ErrorResponseDto();
//		err.setMessage(ex.getMessage());
//		err.setStatus("FAILED");
//		log.error("Access denied", ex);
//		return ResponseEntity.status(HttpStatus.FORBIDDEN).body(err);
//	}

	@ExceptionHandler(RuntimeException.class)
	protected ResponseEntity<ErrorResponseDto> handleRuntimeException(Exception ex, WebRequest request) {
		ErrorResponseDto err = new ErrorResponseDto();
		err.setMessage(ex.getMessage());
		err.setStatus("FAILED");
		log.error("error occured", ex);
		return ResponseEntity.badRequest().body(err);
	}
}

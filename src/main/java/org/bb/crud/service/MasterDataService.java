package org.bb.crud.service;

import java.util.ArrayList;
import java.util.List;

import org.bb.crud.model.mysql.Station;
import org.bb.crud.repository.mysql.StationMasterRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MasterDataService {

	Logger log = LoggerFactory.getLogger(MasterDataService.class);

	@Autowired
	private StationMasterRepository stationMasterRepository;
	
	public List<Station> getAllStation() {
		Iterable<Station> allRecords = stationMasterRepository.findAll();
		List<Station> stationMasterList = new ArrayList<Station>();
		allRecords.forEach(stationMasterList::add);
		log.debug("stationMasterList: " + stationMasterList);
		return stationMasterList;
	}
}

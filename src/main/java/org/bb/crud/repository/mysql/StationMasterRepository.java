package org.bb.crud.repository.mysql;

import org.bb.crud.model.mysql.Station;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StationMasterRepository extends PagingAndSortingRepository<Station, Integer> {
}
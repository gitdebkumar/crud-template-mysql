package org.bb.crud.api;

import org.bb.crud.service.MasterDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest({ StationMasterRestController.class })
public class StationMasterRestControllerTest {

	@Autowired
	MockMvc mockMvc;

	@Autowired
	ObjectMapper mapper;

	@MockBean
	MasterDataService masterDataService;

//	@Test
//	public void getMasterData_success() throws Exception {
//		Mockito.when(masterDataService.getAllStation()).thenReturn();
//
//		mockMvc.perform(MockMvcRequestBuilders.get("/master/station"))
//				.andExpect(MockMvcResultMatchers.status().isOk())
//				.andExpect(MockMvcResultMatchers.content().co)
//				.andExpect(MockMvcResultMatchers.jsonPath("$.masterDataId", Matchers.is("Sam")));
//	}
}
